#!/bin/bash

echo "Digite um número:"
read -r n1

echo "Digite um número:"
read -r n2

echo "Digite a operação (+, -, *, /):"
read -r op

math=$(echo "$n1 $op $n2" | bc)

echo

echo "$n1 $op $n2 = $math"
sleep 2
echo "Resultado: $math"
